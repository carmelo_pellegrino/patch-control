#!/bin/bash
#
# TriDAS launcher
#
# This script allows the launch the TriDAS processes on the machine
#
# Author: Carmelo Pellegrino <carmelo.pellegrino@bo.infn.it>
#

if [ $# -ne 7 ]; then
  echo "ERROR:"
  echo "Syntax: $0 <run_number> <detector_id> <run_start_time> <run_duration_in_sec> <ip of the DAQ server> <username> <writetimeslices>"
  exit -1
fi

RUN_NUMBER=$1
DETECTOR_ID=$2
RUN_START_TIME=$3
RUN_DURATION=$4
DAQ_SERVER=$5
DAQ_USER=$6
WRITE_TIMESLICES=$7

SERVER=${DAQ_SERVER} # JLigier for StateMachine
LOGGER=${DAQ_SERVER} # JLigier for logging
DEBUG=3          # 3 is highest
TIMEOUT_S=20     # timeout arbitrarily set to 20 seconds

NUMBER_OF_FRAMES=18 # 18 doms

DIR=. # where to write the data

DATAFILTER=${DAQ_SERVER} # hostname of oDataFilter
DATAQUEUE=${DAQ_SERVER}  # hostname of DataQueue
PORT=5556            # port to which the dataqueue will send the frames

DATAWRITER=${DAQ_SERVER} # hostname of the DataWriter

DETECTOR=du1cppm.detx # position of the detector file

ROOT_FILE=`printf "${DIR}/KM3NeT_%08d.root" ${RUN_NUMBER}`

# Run number formatted for filenames
RNUMB_FNAMES=`printf "%08d" ${RUN_NUMBER}`

if [ -f ${ROOT_FILE} ] ; then
    echo "The file ${ROOT_FILE} already exists, please rename or remove it first, then restart this script"
    exit -1
fi

# Below starts ligier (put if to 1), but you'd better have it running already
if (( 0 )); then
  JLigier.sh ${SERVER} start
fi

cat>ev_configure_dfilter_dqdf.txt<<EOF
numberOfFramesPerSlice = ${NUMBER_OF_FRAMES};
dataWriter             = ${DATAWRITER}
detector               = %<${DETECTOR}>%;
triggerParameters      = trigger3DMuon.enabled   = 1;
triggerParameters      = trigger3DShower.enabled = 1;
triggerParameters      = writeSummary            = 1;
triggerParameters      = writeTimeslices         = ${WRITE_TIMESLICES};
bufferSize             = 1073741824;
EOF

cp ev_configure_dfilter_dqdf.txt ${HOME}/.km3_private/ev_configure_dfilter_dqdf_R${RNUMB_FNAMES}.txt

cat>ev_configure_dqueue_dqdf.txt<<EOF
timeslice_duration=100;
run_start_time=${RUN_START_TIME};
max_dump_size=1073741824;
dump_file_prefix=dump_;
dump_file_postfix=.dqd;
opto_ports=56015;
acou_ports=56016;
opto_recipients=127.0.0.1:5556;
acou_recipient=127.0.0.1:5800;
EOF

cp ev_configure_dqueue_dqdf.txt ${HOME}/.km3_private/ev_configure_dqueue_dqdf_R${RNUMB_FNAMES}.txt

# Driver input

cat>driver.txt<<EOF

process  DataQueue/1    $DATAQUEUE   ssh  $DAQ_USER@\$HOST\$  "DataQueue -u \$NAME\$  -H \$SERVER\$  -M \$LOGGER\$  -d $DEBUG -o -a </dev/null >&/dev/null &";

process  JDataFilter/1  $DATAFILTER  ssh  $DAQ_USER@\$HOST\$  "JDataFilter  -u \$NAME\$  -P $PORT  -H \$SERVER\$  -M \$LOGGER\$  -d $DEBUG </dev/null >&/dev/null &";

process  JDataWriter    $DATAWRITER  ssh  $DAQ_USER@\$HOST\$  "JDataWriter  -u \$NAME\$  -H \$SERVER\$  -M \$LOGGER\$  -d $DEBUG  </dev/null >&/dev/null &";

enter

print

event ev_init      { RC_CMD }

event ev_configure {
              RC_DQUE %<ev_configure_dqueue_dqdf.txt>%
              RC_DFLTR %<ev_configure_dfilter_dqdf.txt>%
              RC_DWRT path=$DIR; update_s=10; logger_s=5;
}

event ev_start     { RC_CMD ${RUN_NUMBER} ${DETECTOR_ID} }

sleep ${RUN_DURATION}

event ev_pause     { RC_CMD }
event ev_stop      { RC_CMD }
event ev_reset     { RC_CMD }
event ev_off       { RC_CMD }

EOF

cp driver.txt ${HOME}/.km3_private/driver_R${RNUMB_FNAMES}.txt

JDAQDriver -H $SERVER -M $LOGGER -d $DEBUG -t $TIMEOUT_S -f driver.txt -c

exit 0
